<?php
    include_once 'PDO/db_conexao.php';    
    
        $method = $_SERVER['REQUEST_METHOD'];
        //Consulta via Data Inicial.
        
        if($method == "GET"){
            if($_GET['data_ini']){
                //Validar dados de entrada.
                if ( strlen($_GET['data_ini']) != 8){
                    echo 'Data Invalida';
                    exit();
                }else{
                    // pega o dia da data
                    $dia_ini = substr($_GET['data_ini'], -8, 2);
                    // pega o mês da data
                    $mes_ini = substr($_GET['data_ini'], -6,2);
                    // pega o ano da data
                    $ano_ini = substr($_GET['data_ini'], -4,4);
                    if (!checkdate($mes_ini, $dia_ini, $ano_ini)){
                         echo 'Data Invalida';
                         exit();
                    }
                }
            }
            if($_GET['data_fim']){
                //Validar dados de entrada.
                if ( strlen($_GET['data_fim']) != 8){
                    echo 'Data Invalida';
                    exit();
                }else{
                    // pega o dia da data
                    $dia_fim = substr($_GET['data_fim'], -8, 2);
                    // pega o mês da data
                    $mes_fim = substr($_GET['data_fim'], -6,2);
                    // pega o ano da data
                    $ano_fim = substr($_GET['data_fim'], -4,4);
                    if (!checkdate($mes_fim, $dia_fim, $ano_fim)){
                         echo 'Data Invalida';
                         exit();
                    }
                }
            }
         //Converter data para yyyy-mm-dd
         $data_ini = $ano_ini."-".$mes_ini."-".$dia_ini;   
         $data_fim = $ano_fim."-".$mes_fim."-".$dia_fim;   
         /*
         *Realizar consulta para retorno das informações de relatórios.  
         */ 
        $sql = "
                SELECT A.uniqueid as identificador_chamada, QA.fullname as operador,/*Nome do Operador*/ 
                @ddd_src := substring(A.src,1,2) as ddd_src,/*DDD utilizado pelo campo src*/ 
                A.src as src, /*Ligacoes entrantes*/ 
                    case /*Determinar UF via DDD*/ 
                        when @ddd_src IN (11,12,13,14,15,16,17,18,19) then 'SP' 
                        when @ddd_src IN (21,22,24) then 'RJ' 
                        when @ddd_src IN (27,28) then 'ES' 
                        when @ddd_src IN (31,32,33,34,35,37,38) then 'MG' 
                        when @ddd_src IN (41,42,43,44,45,46) then 'PR' 
                        when @ddd_src IN (47,48,49) then 'SC' 
                        when @ddd_src IN (51,53,54,55) then 'RS' 
                        when @ddd_src IN (61) then 'DF' 
                        when @ddd_src IN (63) then 'TO' 
                        when @ddd_src IN (62,64) then 'GO' 
                        when @ddd_src IN (65,66) then 'MT' 
                        when @ddd_src IN (67) then 'MS' 
                        when @ddd_src IN (68) then 'AC' 
                        when @ddd_src IN (69) then 'RO' 
                        when @ddd_src IN (71,73,74,75,77) then 'BA' 
                        when @ddd_src IN (79) then 'SE' 
                        when @ddd_src IN (81,87) then 'PE' 
                        when @ddd_src IN (82) then 'AL' 
                        when @ddd_src IN (83) then 'PB' 
                        when @ddd_src IN (84) then 'RN' 
                        when @ddd_src IN (85,88) then 'CE' 
                        when @ddd_src IN (86,89) then 'PI' 
                        when @ddd_src IN (91,93,94) then 'PA' 
                        when @ddd_src IN (92,97) then 'AM' 
                        when @ddd_src IN (95) then 'RR' 
                        when @ddd_src IN (96) then 'AP' 
                        when @ddd_src IN (98,99) then 'MA' 
                    end as uf_entrantes,     
                    case when length(A.dst) <= 4 then 0
                    else        
                        @ddd_dst := substring(A.dst,1,2) 
                        end as ddd_dst,/*DDD ligacoes saintes*/
                        A.dst as dst, /*Numeros saintes*/
                        case /*Determinar UF via DDD*/
                            when length(A.dst) <= 4 then 0
                            when @ddd_dst IN (11,12,13,14,15,16,17,18,19) then 'SP'
                            when @ddd_dst IN (21,22,24) then 'RJ'
                            when @ddd_dst IN (27,28) then 'ES'
                            when @ddd_dst IN (31,32,33,34,35,37,38) then 'MG' 
                            when @ddd_dst IN (41,42,43,44,45,46) then 'PR'
                            when @ddd_dst IN (47,48,49) then 'SC'
                            when @ddd_dst IN (51,53,54,55) then 'RS'	
                            when @ddd_dst IN (61) then 'DF'
                            when @ddd_dst IN (63) then 'TO'
                            when @ddd_dst IN (62,64) then 'GO'
                            when @ddd_dst IN (65,66) then 'MT'
                            when @ddd_dst IN (67) then 'MS'
                            when @ddd_dst IN (68) then 'AC'
                            when @ddd_dst IN (69) then 'RO'
                            when @ddd_dst IN (71,73,74,75,77) then 'BA'
                            when @ddd_dst IN (79) then 'SE'
                            when @ddd_dst IN (81,87) then 'PE'
                            when @ddd_dst IN (82) then 'AL'
                            when @ddd_dst IN (83) then 'PB'
                            when @ddd_dst IN (84) then 'RN'
                            when @ddd_dst IN (85,88) then 'CE'
                            when @ddd_dst IN (86,89) then 'PI'
                            when @ddd_dst IN (91,93,94) then 'PA'
                            when @ddd_dst IN (92,97) then 'AM'
                            when @ddd_dst IN (95) then 'RR'
                            when @ddd_dst IN (96) then 'AP'
                            when @ddd_dst IN (98,99) then 'MA'
                    end as uf_saidas, /*UF para ligações ativas*/   
                    date_format(str_to_date(substring(A.calldate,1,10), '%Y-%m-%d'), '%d/%m/%Y') as data, /*Inicio da ligacao na URA*/ 
                    @hora_inicio_ura := substring(A.calldate,11,6) as hora_inicio_ura, /*Selecionando somente o tempo HH:MM/ 
                    /*==== Mostrar horario que a ligacao saiu da URA ====*/ 
                    @hora_final_ura := case 
                        when Q.qevent = 16 then 
                            /*Verifica se houve transferencia*/ 
                            substring(sec_to_time((time_to_sec(@hora_inicio_ura) + (A.billsec - ( case when @tempo_fila IS NULL THEN 0 ELSE @tempo_fila end + case when @tempo_atendimento IS NULL THEN 0 ELSE @tempo_atendimento end)))),1,5) 
                        else 
                            /*Caso NAO haja transferencia*/ 
                            substring(sec_to_time((time_to_sec(@hora_inicio_ura) + (A.billsec - ( case when Q.info1 IS NULL THEN 0 ELSE Q.info1 end + case when Q.info2 IS NULL THEN 0 ELSE Q.info2 end)))),1,5) 
                        end as hora_final_ura, 
                /*========================================================*/ 
                /*==== Calculando o tempo de navegacao na URA ====*/ 
                @tempo_navegacao_ura := case when Q.qevent = 16 then 
                        /*Veririca se houve transferencia*/ 
                        (A.billsec - (case when @tempo_fila IS NULL THEN 0 ELSE @tempo_fila end + case when @tempo_atendimento IS NULL THEN 0 ELSE @tempo_atendimento end)) 
                        else 
                        /*Caso NAO haja transferencia*/ 
                        (A.billsec - (case when Q.info1 IS NULL THEN 0 ELSE Q.info1 end + case when Q.info2 IS NULL THEN 0 ELSE Q.info2 end)) 
                end as tempo_navegacao_ura, 
                /*========================================================*/ 
                case 
                    when Q.qevent IN (1,7,8,16) THEN 
                        'SIM' 
                    ELSE 
                        'NAO' 
                end as Encaminhada_atendimento, 
                /*Validando se a ligacao foi direcionada para fila de atendimento*/ 
                /*==== Mostrar horario que a ligacao iniciou na FIla=====*/ 
                @hora_final_ura AS hora_inicio_fila, 
                /*==========================================================*/ 
                /*==== Horario que a ligacao saiu da fila de espera ====*/ 
                @hora_final_fila := case 
                    when Q.qevent = 16 then 
                        substring(sec_to_time((time_to_sec(@hora_inicio_ura) + (@tempo_fila + @tempo_navegacao_ura))),1,5) 
                    ELSE 
                        substring(sec_to_time((time_to_sec(@hora_inicio_ura) + (Q.info1 + @tempo_navegacao_ura))),1,5) end as hora_final_fila, 
                /*=========================================================*/ 
                /*==== Tempo de espera em fila ====*/ 
                case when Q.qevent = 16 then 
                    @tempo_fila := substring_index(Q.info3,'|',1) 
                ELSE 
                    Q.info1 
                end as tempo_fila, 
                /*================================ */ 
                case Q.qevent /*Verificar se foi gerado abandono na fila*/ when 1 then 
                    'SIM' 
                ELSE 
                    'NAO' 
                end as ligacao_encerrada_fila, 
                /*==== Hora que a ligacao foi atendida pelo operador ====*/ 
                @hora_final_fila as hora_inicio_atendimento, 
                /*=========================================================*/ 
                case when Q.qevent = 1 then 0 
                ELSE 
                    case when Q.qevent = 16 then 
                        case when @tempo_atendimento IS NULL THEN 0 
                        ELSE substring(sec_to_time(time_to_sec(@hora_inicio_ura) + (@tempo_atendimento + @tempo_fila + @tempo_navegacao_ura)),1,5) 
                        end 
                        ELSE 
                        case when Q.info2 IS NULL THEN 0 
                        ELSE substring(sec_to_time(time_to_sec(@hora_inicio_ura) + (Q.info2 + Q.info1 + @tempo_navegacao_ura)),1,5) 
                        end 
                    end 
                end as hora_final_atendimento, 
                /*Hora que a ligacao foi encerrada no atendimento*/ 
                /*===================================================================================*/ 
                /*==== Tempo de atendimento da ligacao ====*/ 
                case when Q.qevent = 1 then 0 
                else 
                    case when Q.qevent = 16 then 
                        @tempo_atendimento := substring_index(substring_index(Q.info3, '|', 2), '|', -1) 
                    else Q.info2 
                        end 
                end as tempo_atendimento_humano,
                /*Tempo de atendimento da ligacao*/ 
                 /*==== Verifica Origem da chamada ====*/
                case when A.dst = 's' then 
                    'RECEPTIVO'
                ELSE 
                    'ATIVO'
                end as origem_chamada
                /*====================================*/
                /*==========================================*/ 
                FROM `vip_cdr` A LEFT JOIN `queue_stats` Q ON Q.uniqueid = A.uniqueid 
                LEFT JOIN `qagent` QA ON Q.qagent = QA.agent_id 
                WHERE A.calldate BETWEEN '2020-04-01 00:00:00' AND '2020-05-07 23:59:59' 
                AND A.dcontext in ('entrada-bny','saida-bny')
                AND (qevent in (1,7,8,16) or qevent is null) 
                /*Eventos para consultas: 1 - Abandono, 7 - Chamada direcionada e desligada pelo Agente, 8 - Chamada direcionada e desligada pelo cliente*/ 
                ORDER BY calldate DESC";
        
        //print_r($sql);exit();
        
        $result = mysqli_query($conn, $sql);
        $rows = array();
        
        if(mysqli_num_rows($result) > 0){
            while($r = mysqli_fetch_assoc($result)){
                array_push($rows, $r);
            }            
            print json_encode($rows);//Mostrando resgistros do banco.
        }else{
            echo "NO DATA";
        }
    }else{
        echo "Metodo Invalido";
    }
        //Encerrando a conexão com o banco
        mysqli_close();    

?>
	<!--
                    <h1>Este endereço foi alterado para melhora de performance</h1>
	<h2>Favor acessar através dos endereços abaixo:</h2>
	<p>
                                        Para acesso externo: <a href="http://191.33.76.242:8381/ws-anac/index.php?"></a>+Parametros
	</p>
	<p>
		Para acesso local: <a href="http://ws-vector/ws-anac/index.php?"></a>+Parametros
	</p>
                    -->
        
